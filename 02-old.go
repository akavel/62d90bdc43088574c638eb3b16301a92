func (t *X) initializeAndActivate(ctx context.Context) error {
	t.logger.Info(fmt.Sprintf("activating X for transaction [%q] @%s", xxx.TransactionCodeFrom(ctx), t.millis()))

	t.bus.Lock()
	defer t.bus.Unlock()
	if t.fooing {
		return fmt.Errorf("trying to activate X for transaction %q while already active", xxx.TransactionCodeFrom(ctx))
	}

	response, err := t.queryOnly(cmd_foo1)
	if err != nil {
		t.hardReset()
		return err
	}
	if response[0] != cmd_foo1[0] {
		t.hardReset()
		return fmt.Errorf("invalid response to FOO1 [% 02X]", response)
	}
	t.fooTable, err = parseFooTable(response[1:])
	if err != nil {
		t.hardReset()
		return err
	}
	t.logger.Info(fmt.Sprintf("X parsed foo table [% 02x] as: %s", response, yyy.JsonString(t.fooTable)))

	err = t.queryAndExpect(cmd_foo2, cmd_foo2)
	if err != nil {
		t.hardReset()
		return err
	}
	err = t.queryAndExpect(cmd_foo3, cmd_foo3)
	if err != nil {
		t.hardReset()
		return err
	}
	err = t.queryAndExpect(cmd_foo4, cmd_foo4)
	if err != nil {
		t.hardReset()
		return err
	}

	t.logger.Info(fmt.Sprintf("activated X for transaction [%q] @%s", xxx.TransactionCodeFrom(ctx), t.millis()))
	t.fooing = true
	return nil
}
