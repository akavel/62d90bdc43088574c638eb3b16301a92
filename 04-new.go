func (t *X) initializeAndActivate_Go2(ctx context.Context) error {
	t.logger.Info(fmt.Sprintf("activating X for transaction [%q] @%s", xxx.TransactionCodeFrom(ctx), t.millis()))

	t.bus.Lock()
	defer t.bus.Unlock()
	if t.fooing {
		return fmt.Errorf("trying to activate X for transaction %q while already active", xxx.TransactionCodeFrom(ctx))
	}

	handle err {
		t.hardReset()
		return err
	}
	response := check t.queryOnly(cmd_foo1)
	if response[0] != cmd_foo1[0] {
		check fmt.Errorf("invalid response to FOO1 [% 02X]", response)
	}
	t.fooTable = check parseFooTable(response[1:])
	t.logger.Info(fmt.Sprintf("X parsed foo table [% 02x] as: %s", response, yyy.JsonString(t.fooTable)))

	check t.queryAndExpect(cmd_foo2, cmd_foo2)
	check t.queryAndExpect(cmd_foo3, cmd_foo3)
	check t.queryAndExpect(cmd_foo4, cmd_foo4)

	t.logger.Info(fmt.Sprintf("activated X for transaction [%q] @%s", xxx.TransactionCodeFrom(ctx), t.millis()))
	t.fooing = true
	return nil
}
